const gcs = new require("@google-cloud/storage")({
    projectId: "orbital-ability-170112",
    keyFilename: "./key.json"
});

const multiparty = require("multiparty");
const requestProxy = require("express-request-proxy");
const morgan = require("morgan");

const express = require("express");
const app = express();

const BUCKET_NAME = "usersfiles";

app.use(morgan('combined'))
app.use(express.static("public"));
app.disable('etag');

app.get("/file/:fileName", requestProxy({
    url: `https://storage.googleapis.com/${BUCKET_NAME}/:fileName`
}));

app.get("/list", (req, res, next) => {
    gcs.bucket(BUCKET_NAME)
        .getFiles()
        .then(([files]) => {
            res.status(200).json(files.map(f => f.metadata));
        }, next);
});

app.post("/upload", (req, res, next) => {
    let form = new multiparty.Form({
        autoFields: false,
        autoFiles: false
    });

    let uploaded = [];

    form.on("error", next);

    form.on("part", part => {
        uploaded.push({ filename: part.filename, fieldName: part.name });

        let fileWriteStream = gcs.bucket(BUCKET_NAME)
            .file(part.filename)
            .createWriteStream({ public: true });

        part
            .pipe(fileWriteStream);
        

        part.on("error", next);
    });

    form.on("close", () => {
        console.log("Uploaded files", uploaded);
        setTimeout(() => {
            res.redirect("/");
        }, 1000);
    });

    form.parse(req);
});

app.delete("/delete/:filename", (req, res, next) => {
    gcs.bucket(BUCKET_NAME)
        .file(req.params.filename)
        .delete()
        .then(() => {
            res.status(200).end();
        }, next);
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}!`);
});